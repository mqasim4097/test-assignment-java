package ee.energia.testassignment;

import ee.energia.testassignment.planning.ChargePlan;
import ee.energia.testassignment.price.EnergyPrice;

import java.util.ArrayList;
import java.util.Collections;

public class ChargePlanner {

    // the capability of the charger to charge certain amount of energy into the battery in 1 hour
    public final static int CHARGER_POWER = 50;

    // maximum battery level possible
    public final static int MAX_LEVEL = 100;

    // battery level required by the end of the charging
    public final static int REQUIRED_LEVEL = 100;

    /**
     * Method calculates the optimal hourly charge plan.
     * Method finds the cheapest hour to charge the battery (if multiple then the earliest)
     * and uses it to charge the battery up to the {@link ChargePlanner#REQUIRED_LEVEL}.
     * If {@link ChargePlanner#CHARGER_POWER} limitation does not allow to do this in one hour,
     * then method finds the next cheapest opportunities and uses them until {@link ChargePlanner#REQUIRED_LEVEL} is met.
     *
     * Method returns the array of {@link ChargePlan} objects that represent the hourly time slot
     * and the capacity that we need to charge during that hour to charge the battery.
     *
     * @param batteryLevel initial battery level when the charger is connected
     * @param energyPrices the list of the energy prices from the moment when charger is connected until the moment when battery needs to be charged
     *                     there is an assumption that battery is connected the first second of the first given hour and disconnected the last second of the last given hour
     * @return
     */
    public static ArrayList<ChargePlan> calculateChargePlan(int batteryLevel, ArrayList<EnergyPrice> energyPrices) {
        // TODO: implement the function that will be calculating the optimal hourly charge plan
    	Collections.sort(energyPrices);
    	final ArrayList<ChargePlan> chargePlanList = new ArrayList<>();
		
    	for (EnergyPrice price : energyPrices) { 
    		
			int capacity = Math.min(ChargePlanner.REQUIRED_LEVEL - batteryLevel, ChargePlanner.CHARGER_POWER);
			if(capacity + batteryLevel <= ChargePlanner.REQUIRED_LEVEL && batteryLevel != ChargePlanner.REQUIRED_LEVEL) {
				chargePlanList.add(new ChargePlan(capacity, price.getHour(), price.getMonth(), price.getYear()));
				batteryLevel = capacity + batteryLevel;
			}
			else {
				chargePlanList.add(new ChargePlan(0, price.getHour(), price.getMonth(), price.getYear()));
			}
		}
		Collections.sort(chargePlanList);
        return chargePlanList;
    }
    
    public static int calculateSavedAmount(int batteryLevel, ArrayList<EnergyPrice> energyPrices) {
    	// This function will calculate both the optimized price for the charge plan and the classical way of charging prices
    	// and return the amount that is saved from doing the optimized approach
    	
    	final ArrayList<ChargePlan> chargePlanList = new ArrayList<>();
    	final ArrayList<EnergyPrice> selectedEnergyPrices = new ArrayList<>();
		int non_optimized_price = 0;
    	for (EnergyPrice price : energyPrices) { 
    		
			int capacity = Math.min(ChargePlanner.REQUIRED_LEVEL - batteryLevel, ChargePlanner.CHARGER_POWER);
			if(capacity + batteryLevel <= ChargePlanner.REQUIRED_LEVEL && batteryLevel != ChargePlanner.REQUIRED_LEVEL) {
				non_optimized_price = non_optimized_price + price.getBidPrice();
				batteryLevel = capacity + batteryLevel;
			}
		}
    	Collections.sort(energyPrices);
    	int optimized_price = 0;
    	for (EnergyPrice price : energyPrices) { 
    		
			int capacity = Math.min(ChargePlanner.REQUIRED_LEVEL - batteryLevel, ChargePlanner.CHARGER_POWER);
			if(capacity + batteryLevel <= ChargePlanner.REQUIRED_LEVEL && batteryLevel != ChargePlanner.REQUIRED_LEVEL) {
				optimized_price = optimized_price + price.getBidPrice();
				batteryLevel = capacity + batteryLevel;
			}
		}
    	int saved_amount = non_optimized_price - optimized_price;
        return saved_amount;
    }
    
}
